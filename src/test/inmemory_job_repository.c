#include <stdio.h>
#include <string.h>

#include "domain/job_repository.h"
#include "adapter/inmemory_job_repository.h"


int main() {
    JobRepository* repository = (JobRepository*) new_InMemoryJobRepository();
    construct_InMemoryJobRepository((InMemoryJobRepository*) repository);

    Job testJob;
    construct_Job(&testJob);
    char buffer[1000];

    for(int i=0; i<5; i++) {
        int t = i*6;
        testJob.salary = t;
        sprintf(buffer, "test job %d", t);
        construct_String(&testJob.title, buffer);
        construct_String(&testJob.location, "tashkent");
        construct_String(&testJob.description, buffer);
        repository->pCreate(repository, &testJob);

    }

    Job resultJob;
    resultJob.id = 2;
    repository->pRead(repository, &resultJob);
    printf("%s\n", resultJob.title.str);
    printf("%s\n", resultJob.location.str);
}