#include <stdlib.h>

#include "inmemory_job_repository.h"
#include <string.h>

InMemoryJobRepository* new_InMemoryJobRepository()
{
    InMemoryJobRepository* pObject = (InMemoryJobRepository*) malloc(sizeof(InMemoryJobRepository));
    return pObject;
}
void construct_InMemoryJobRepository(InMemoryJobRepository* const pJobRepositoryObject) {
    pJobRepositoryObject->jobList = 0;
    construct_JobRepository(
        &pJobRepositoryObject->pBaseObject,
        (JobRepository_CreatePtr) InMemoryJobRepository_Create,
        (JobRepository_ReadPtr) InMemoryJobRepository_Read,
        (JobRepository_ReadAllPtr) InMemoryJobRepository_ReadAll,
        (JobRepository_UpdatePtr) InMemoryJobRepository_Update,
        (JobRepository_DestructPtr) InMemoryJobRepository_Delete,
        (DestructPtr) destruct_InMemoryJobRepository
    );

}
void destruct_InMemoryJobRepository(InMemoryJobRepository* const pJobRepositoryObject) {
    JobNode* current = pJobRepositoryObject->jobList;
    while (current!=0) {
        destruct_Destructable((Destructable*) current);
        current = current->next;
    }
}

void InMemoryJobRepository_Create(InMemoryJobRepository* const pJobRepositoryObject, Job* pJob) {
    JobNode* previous = 0;
    JobNode* current = pJobRepositoryObject->jobList;
    JobNode** previous_next = &pJobRepositoryObject->jobList;
    while (current!=0) {
        previous = current;
        previous_next = &current->next;
        current = current->next;
    }
    int newId = 0;
    if(previous!=0) {
        newId = previous->current.id + 1;
    }
    pJob->id = newId;
    JobNode* newNode = new_JobNode();
    construct_JobNode(newNode, pJob);
    *previous_next = newNode;
}

void InMemoryJobRepository_Read(InMemoryJobRepository* const pJobRepositoryObject, Job* pJob) {
    JobNode* current = pJobRepositoryObject->jobList;
    while (current!=0) {
        if(current->current.id==pJob->id) {
            copy_Job(pJob, &current->current);
            return;
        }
        current = current->next;
    }
}
void InMemoryJobRepository_ReadAll(InMemoryJobRepository* const pJobRepositoryObject, Job* pJob, JobNode** resultingList) {
    JobNode* current = pJobRepositoryObject->jobList;
    *resultingList = 0;
    JobNode* resultCurrent = 0;
    JobNode* tmp;
    while (current!=0) {
        if(strcmp(current->current.title.str, pJob->title.str)==0) {
            tmp = resultCurrent;
            resultCurrent = new_JobNode();
            if(tmp==0) {
                *resultingList = resultCurrent;
            } else {
                tmp->next = resultCurrent;
            }
            //copy_Job(&resultCurrent->current, &current->current);
            construct_JobNode(resultCurrent, &current->current);
        }
        current = current->next;
    }
}
void InMemoryJobRepository_Update(InMemoryJobRepository* const pJobRepositoryObject, Job* pJob) {
    JobNode* current = pJobRepositoryObject->jobList;
    while (current!=0) {
        if(current->current.id==pJob->id) {
            copy_Job(pJob, &current->current);
            break;
        }
        current = current->next;
    }
}
void InMemoryJobRepository_Delete(InMemoryJobRepository* const pJobRepositoryObject, Job* pJob) {
    JobNode* previous = 0;
    JobNode* current = pJobRepositoryObject->jobList;
    JobNode** previous_next = &pJobRepositoryObject->jobList;
    while (current!=0) {
        if(current->current.id==pJob->id) {
            *previous_next = current->next;
            destruct_Destructable((Destructable*) current);
            delete_JobNode(current);
            break;
        }
        previous = current;
        previous_next = &current->next;
        current = current->next;
    }
}