#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include "linux_socket.h"

#define PORT 8080

LinuxSocket* new_LinuxSocket()
{
    LinuxSocket* pLinuxSocketObject = malloc(sizeof(LinuxSocket));
    return pLinuxSocketObject;
};
void construct_LinuxSocket(LinuxSocket* pLinuxSocketObject, int fd) {
    pLinuxSocketObject->fd = fd;
    construct_Socket(
        &pLinuxSocketObject->pBaseObject,
        (RecvPtr) LinuxSocket_recv,
        (SendPtr) LinuxSocket_send,
        (DestructPtr) destruct_LinuxSocket
    );
}

void destruct_LinuxSocket(LinuxSocket* const pLinuxSocketObject) {
    close(pLinuxSocketObject->fd);
    //destruct_Socket(&pLinuxSocketObject->pBaseObject);
}

int LinuxSocket_recv(LinuxSocket* const pLinuxSocketObject, void* buffer, int buffer_length) {
    return read(pLinuxSocketObject->fd, buffer, buffer_length);
}
int LinuxSocket_send(LinuxSocket* const pLinuxSocketObject, void* buffer, int buffer_length){
    send(pLinuxSocketObject->fd, buffer, buffer_length, 0);
}