#ifndef LINUX_SOCKET_H
#define LINUX_SOCKET_H

#include "domain/socket.h"

#include <sys/socket.h>
#include <netinet/in.h>

typedef struct _LinuxSocket
{
    Socket pBaseObject;
    int fd;
} LinuxSocket;

LinuxSocket* new_LinuxSocket();
void construct_LinuxSocket(LinuxSocket* pLinuxSocketObject, int fd);
void destruct_LinuxSocket(LinuxSocket* const);

int LinuxSocket_recv(LinuxSocket* const pLinuxSocketObject, void* buffer, int buffer_length);
int LinuxSocket_send(LinuxSocket* const pLinuxSocketObject, void* buffer, int buffer_length);

#endif