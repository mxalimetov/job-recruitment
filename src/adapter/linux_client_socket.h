#ifndef LINUX_CLIENT_SOCKET_H
#define LINUX_CLIENT_SOCKET_H

#include "domain/client_socket.h"

#include <sys/socket.h>
#include <netinet/in.h>

typedef struct _LinuxClientSocket
{
    ClientSocket pBaseObject;
    int fd;
    struct sockaddr_in address;
    int opt;
} LinuxClientSocket;

LinuxClientSocket* new_LinuxClientSocket();
void construct_LinuxClientSocket(LinuxClientSocket* pLinuxClientSocketObject);
void destruct_LinuxClientSocket(LinuxClientSocket* const pLinuxClientSocketObject);

void LinuxClientSocket_connect(LinuxClientSocket* pLinuxClientSocketObject, char* host, int port);

#endif