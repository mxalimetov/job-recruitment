#ifndef PROXY_SOCKET_H
#define PROXY_SOCKET_H

#include "domain/socket.h"

typedef struct _ProxySocket ProxySocket;

typedef struct _ProxySocket
{
    Socket pBaseObject;
    Socket* pSocket;
} ProxySocket;

ProxySocket* new_ProxySocket();
void construct_ProxySocket(ProxySocket* const pProxySocketObject, Socket* pSocket, DestructPtr destruct);
void destruct_ProxySocket(ProxySocket* const pProxySocketObject);

int ProxySocket_recv(ProxySocket* const pProxySocketObject, void* buffer, int buffer_length);
int ProxySocket_send(ProxySocket* const pProxySocketObject, void* buffer, int buffer_length);
#endif