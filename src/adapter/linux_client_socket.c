#include <stdio.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <unistd.h>

#include "linux_client_socket.h"
#include "linux_socket.h"


LinuxClientSocket* new_LinuxClientSocket()
{
    LinuxClientSocket* pLinuxClientSocketObject = malloc(sizeof(LinuxClientSocket));
    return pLinuxClientSocketObject;
};

void construct_LinuxClientSocket(LinuxClientSocket* pLinuxClientSocketObject) {
    pLinuxClientSocketObject->fd = socket(AF_INET, SOCK_STREAM, 0);
    if (pLinuxClientSocketObject->fd < 0) {
        perror("socket failed");
        return;
    }
    LinuxSocket* pLinuxSocketObject = new_LinuxSocket();
    construct_LinuxSocket(pLinuxSocketObject, pLinuxClientSocketObject->fd);
    
    construct_ClientSocket(&pLinuxClientSocketObject->pBaseObject, (ConnectPtr) LinuxClientSocket_connect);
    construct_ProxySocket(&pLinuxClientSocketObject->pBaseObject.pBaseObject, &pLinuxSocketObject->pBaseObject, (DestructPtr) destruct_LinuxClientSocket);  
}

void destruct_LinuxClientSocket(LinuxClientSocket* const pLinuxClientSocketObject) {
    close(pLinuxClientSocketObject->fd);
    //destruct_ClientSocket(&pLinuxClientSocketObject->pBaseObject);
}

void LinuxClientSocket_connect(LinuxClientSocket* pLinuxClientSocketObject, char* host, int port) {
    pLinuxClientSocketObject->address.sin_family = AF_INET;
    pLinuxClientSocketObject->address.sin_port = htons(port);

    // Convert IPv4 and IPv6 addresses from text to binary form
    if(inet_pton(AF_INET, host, &pLinuxClientSocketObject->address.sin_addr)<=0) 
    {
        printf("\nInvalid address/ Address not supported \n");
        return;
    }

    if (connect(
        pLinuxClientSocketObject->fd,
        (struct sockaddr *) &pLinuxClientSocketObject->address,
        sizeof(pLinuxClientSocketObject->address)
    ) < 0) {
        printf("\nConnection Failed \n");
        //return 0;
    }
}
