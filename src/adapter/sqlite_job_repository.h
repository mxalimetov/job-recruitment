#ifndef SQLITE_JOB_REPOSITORY_H
#define SQLITE_JOB_REPOSITORY_H

#include <sqlite3.h>
#include "domain/job_repository.h"
//#include "domain/job_node.h"

typedef struct _SQLiteJobRepository
{
    JobRepository pBaseObject;
    sqlite3 *db;
} SQLiteJobRepository;

SQLiteJobRepository* new_SQLiteJobRepository();
void construct_SQLiteJobRepository(SQLiteJobRepository* const pJobRepositoryObject, char* dbUrl);
void destruct_SQLiteJobRepository(SQLiteJobRepository* const pJobRepositoryObject);

void SQLiteJobRepository_Create(SQLiteJobRepository* const pJobRepositoryObject, Job* pJob);
void SQLiteJobRepository_Read(SQLiteJobRepository* const pJobRepositoryObject, Job* pJob);
void SQLiteJobRepository_ReadAll(SQLiteJobRepository* const pJobRepositoryObject, Job* pJob, JobNode** resultingList);
void SQLiteJobRepository_Update(SQLiteJobRepository* const pJobRepositoryObject, Job* pJob);
void SQLiteJobRepository_Delete(SQLiteJobRepository* const pJobRepositoryObject, Job* pJob);

#endif