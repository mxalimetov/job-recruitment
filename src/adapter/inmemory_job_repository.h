#ifndef INMEMORY_JOB_REPOSITORY_H
#define INMEMORY_JOB_REPOSITORY_H

#include "domain/job_repository.h"
//#include "domain/job_node.h"

typedef struct _InMemoryJobRepository
{
    JobRepository pBaseObject;
    JobNode* jobList;
} InMemoryJobRepository;

InMemoryJobRepository* new_InMemoryJobRepository();
void construct_InMemoryJobRepository(InMemoryJobRepository* const pJobRepositoryObject);
void destruct_InMemoryJobRepository(InMemoryJobRepository* const pJobRepositoryObject);

void InMemoryJobRepository_Create(InMemoryJobRepository* const pJobRepositoryObject, Job* pJob);
void InMemoryJobRepository_Read(InMemoryJobRepository* const pJobRepositoryObject, Job* pJob);
void InMemoryJobRepository_ReadAll(InMemoryJobRepository* const pJobRepositoryObject, Job* pJob, JobNode** resultingList);
void InMemoryJobRepository_Update(InMemoryJobRepository* const pJobRepositoryObject, Job* pJob);
void InMemoryJobRepository_Delete(InMemoryJobRepository* const pJobRepositoryObject, Job* pJob);

#endif