#include "proxy_socket.h"
#include <stdlib.h>

ProxySocket* new_ProxySocket() {
    ProxySocket* pObject = (ProxySocket*) malloc(sizeof(ProxySocket));
    return pObject;
}
void construct_ProxySocket(ProxySocket* const pProxySocketObject, Socket* pSocket, DestructPtr destruct) {
    construct_Socket(
        &pProxySocketObject->pBaseObject,
        (RecvPtr) ProxySocket_recv,
        (SendPtr) ProxySocket_send,
        destruct
        //(DestructPtr) destruct_ProxySocket
    );
    pProxySocketObject->pSocket = pSocket;
}
void destruct_ProxySocket(ProxySocket* const pProxySocketObject) {
    //destruct_Socket(pProxySocketObject->pSocket);
    //destruct_Socket(&pProxySocketObject->pBaseObject);
}

int ProxySocket_recv(ProxySocket* const pProxySocketObject, void* buffer, int buffer_length) {
    return pProxySocketObject->pSocket->recv(
        pProxySocketObject->pSocket,
        buffer, buffer_length
    );
}
int ProxySocket_send(ProxySocket* const pProxySocketObject, void* buffer, int buffer_length) {
    return pProxySocketObject->pSocket->send(
        pProxySocketObject->pSocket,
        buffer, buffer_length
    );
}