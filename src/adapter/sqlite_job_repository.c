#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "sqlite_job_repository.h"


SQLiteJobRepository* new_SQLiteJobRepository()
{
    SQLiteJobRepository* pObject = (SQLiteJobRepository*) malloc(sizeof(SQLiteJobRepository));
    return pObject;
}
void construct_SQLiteJobRepository(SQLiteJobRepository* const pJobRepositoryObject, char* dbUrl) {    
    construct_JobRepository(
        &pJobRepositoryObject->pBaseObject,
        (JobRepository_CreatePtr) SQLiteJobRepository_Create,
        (JobRepository_ReadPtr) SQLiteJobRepository_Read,
        (JobRepository_ReadAllPtr) SQLiteJobRepository_ReadAll,
        (JobRepository_UpdatePtr) SQLiteJobRepository_Update,
        (JobRepository_DestructPtr) SQLiteJobRepository_Delete,
        (DestructPtr) destruct_SQLiteJobRepository
    );
    ;
    if (sqlite3_open(dbUrl, &pJobRepositoryObject->db) != SQLITE_OK) {
        printf("Cannot open database: %s\n", sqlite3_errmsg(pJobRepositoryObject->db));
        sqlite3_close(pJobRepositoryObject->db);
        return;
    }
}
void destruct_SQLiteJobRepository(SQLiteJobRepository* const pJobRepositoryObject) {
    sqlite3_close(pJobRepositoryObject->db);
}

void SQLiteJobRepository_Create(SQLiteJobRepository* const pJobRepositoryObject, Job* pJob) {
    // char *err_msg = 0;
    // char sql[2048];
    // sprintf(sql, "INSERT INTO jobs VALUES (%d, %d, '%s', '%s', '%s', '%s');", 
    //     pJob->salary, pJob->experience,
    //     pJob->title, pJob->location, pJob->company, pJob->description
    // );
    // if(sqlite3_exec(pJobRepositoryObject->db, sql, 0, 0, &err_msg)!= SQLITE_OK) {
    //     printf("SQL error: %s\n", err_msg);
    //     sqlite3_free(err_msg);
    // }
    
    sqlite3_stmt *res;
    char *sql = "INSERT INTO jobs (salary, experience, title, location, company, description) VALUES (@salary, @experience, @title, @location, @company, @description);";
    if(sqlite3_prepare_v2(pJobRepositoryObject->db, sql, -1, &res, 0) == SQLITE_OK) {
        sqlite3_bind_int(res, sqlite3_bind_parameter_index(res, "@salary"), pJob->salary);
        sqlite3_bind_int(res, sqlite3_bind_parameter_index(res, "@experience"), pJob->experience);
        sqlite3_bind_text(res, sqlite3_bind_parameter_index(res, "@title"), pJob->title.str, strlen(pJob->title.str), NULL);
        sqlite3_bind_text(res, sqlite3_bind_parameter_index(res, "@location"), pJob->location.str, strlen(pJob->location.str), NULL);
        sqlite3_bind_text(res, sqlite3_bind_parameter_index(res, "@company"), pJob->company.str, strlen(pJob->company.str), NULL);
        sqlite3_bind_text(res, sqlite3_bind_parameter_index(res, "@description"), pJob->description.str, strlen(pJob->description.str), NULL);
    } else {
        printf("Failed to execute statement: %s\n", sqlite3_errmsg(pJobRepositoryObject->db));
    }
    if (sqlite3_step(res) != SQLITE_DONE) {
        printf("Failed to execute statement: %s\n", sqlite3_errmsg(pJobRepositoryObject->db));
    }
    pJob->id = sqlite3_last_insert_rowid(pJobRepositoryObject->db);
    sqlite3_finalize(res);
}

void SQLiteJobRepository_Read(SQLiteJobRepository* const pJobRepositoryObject, Job* pJob) {
    sqlite3_stmt *res;
    char *sql = "SELECT salary, experience, title, location, company, description FROM jobs WHERE id = @id";
    if(sqlite3_prepare_v2(pJobRepositoryObject->db, sql, -1, &res, 0) == SQLITE_OK) {
        sqlite3_bind_int(res, sqlite3_bind_parameter_index(res, "@id"), pJob->id);
    } else {
        printf("Failed to execute statement: %s\n", sqlite3_errmsg(pJobRepositoryObject->db));
    }
    if (sqlite3_step(res) == SQLITE_ROW) {
        pJob->salary = sqlite3_column_int(res, 0);
        pJob->experience = sqlite3_column_int(res, 1);
        //destruct_String(&pJob->title);
        construct_String(&pJob->title, sqlite3_column_text(res, 2) );
        construct_String(&pJob->location, sqlite3_column_text(res, 3) );
        construct_String(&pJob->company, sqlite3_column_text(res, 4) );
        construct_String(&pJob->description, sqlite3_column_text(res, 5) );
        
    } 
    sqlite3_finalize(res);

}
void SQLiteJobRepository_ReadAll(SQLiteJobRepository* const pJobRepositoryObject, Job* pJob, JobNode** resultingList) {
    sqlite3_stmt *res;
    char *sql = "SELECT id, salary, experience, title, location, company, description FROM jobs WHERE title = @title";
    if(sqlite3_prepare_v2(pJobRepositoryObject->db, sql, -1, &res, 0) == SQLITE_OK) {
        sqlite3_bind_text(res, sqlite3_bind_parameter_index(res, "@title"), pJob->title.str, strlen(pJob->title.str), NULL);
    } else {
        printf("Failed to execute statement: %s\n", sqlite3_errmsg(pJobRepositoryObject->db));
    }
    JobNode* resultCurrent = 0;
    JobNode* tmpNode;
    Job tmpJob;
    while (sqlite3_step(res) == SQLITE_ROW) {
        tmpNode = resultCurrent;
        resultCurrent = new_JobNode();
        if(tmpNode==0) {
            *resultingList = resultCurrent;
        } else {
            tmpNode->next = resultCurrent;
        }
        //copy_Job(&resultCurrent->current, &current->current);
        tmpJob.id = sqlite3_column_int(res, 0);
        tmpJob.salary = sqlite3_column_int(res, 1);
        tmpJob.experience = sqlite3_column_int(res, 2);
        //destruct_String(&tmpJob.title);
        construct_String(&tmpJob.title, sqlite3_column_text(res, 3) );
        construct_String(&tmpJob.location, sqlite3_column_text(res, 4) );
        construct_String(&tmpJob.company, sqlite3_column_text(res, 5) );
        construct_String(&tmpJob.description, sqlite3_column_text(res, 6) );
        construct_JobNode(resultCurrent, &tmpJob);
        
    } 
    sqlite3_finalize(res);
}
void SQLiteJobRepository_Update(SQLiteJobRepository* const pJobRepositoryObject, Job* pJob) {

}
void SQLiteJobRepository_Delete(SQLiteJobRepository* const pJobRepositoryObject, Job* pJob) {

}