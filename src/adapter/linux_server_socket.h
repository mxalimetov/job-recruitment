#ifndef LINUX_SERVER_SOCKET_H
#define LINUX_SERVER_SOCKET_H

#include "domain/server_socket.h"

#include <sys/socket.h>
#include <netinet/in.h>

typedef struct _LinuxServerSocket
{
    ServerSocket pBaseObject;
    int fd;
    struct sockaddr_in address;
    int opt;
} LinuxServerSocket;

LinuxServerSocket* new_LinuxServerSocket();
void construct_LinuxServerSocket(LinuxServerSocket* const pLinuxServerSocketObject, int port);
void destruct_LinuxServerSocket(LinuxServerSocket* const pLinuxServerSocketObject);

Socket* LinuxServerSocket_accept(LinuxServerSocket* const pLinuxServerSocketObject);

#endif