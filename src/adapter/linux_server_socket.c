#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "linux_server_socket.h"
#include "linux_socket.h"

LinuxServerSocket* new_LinuxServerSocket()
{
    LinuxServerSocket* pLinuxServerSocketObject = malloc(sizeof(LinuxServerSocket));
    return pLinuxServerSocketObject;
};
void construct_LinuxServerSocket(LinuxServerSocket* const pLinuxServerSocketObject, int port) {
    pLinuxServerSocketObject->fd = socket(AF_INET, SOCK_STREAM, 0);
    if (pLinuxServerSocketObject->fd < 0) {
        perror("socket failed");
        return;
    }

    if (setsockopt(
        pLinuxServerSocketObject->fd,
        SOL_SOCKET,
        SO_REUSEADDR,
        &pLinuxServerSocketObject->opt,
        sizeof(pLinuxServerSocketObject->opt)
    )) {
        perror("setsockopt");
        return;
    }

    pLinuxServerSocketObject->address.sin_family = AF_INET;
    pLinuxServerSocketObject->address.sin_addr.s_addr = INADDR_ANY;
    pLinuxServerSocketObject->address.sin_port = htons(port);

    if (bind(
        pLinuxServerSocketObject->fd,
        (struct sockaddr *) &pLinuxServerSocketObject->address, 
        sizeof(pLinuxServerSocketObject->address)
    )<0) {
        perror("bind failed");
        return;
    }
    if (listen(pLinuxServerSocketObject->fd, 3) < 0){
        perror("listen");
        return;
    }

    ServerSocket* pBaseObject = &pLinuxServerSocketObject->pBaseObject;
    construct_ServerSocket(pBaseObject, (AcceptPtr) LinuxServerSocket_accept, (DestructPtr) destruct_LinuxServerSocket);
    //construct_Destructable(&pBaseObject->destructable, (DestructPtr) destruct_LinuxServerSocket);
}

void destruct_LinuxServerSocket(LinuxServerSocket* const pLinuxServerSocketObject) {
    close(pLinuxServerSocketObject->fd);
    //destruct_ServerSocket(&pLinuxServerSocketObject->pBaseObject);
}

Socket* LinuxServerSocket_accept(LinuxServerSocket* const pLinuxServerSocketObject) {
    int addrlen = sizeof(pLinuxServerSocketObject->address);
    int socket_descriptor = accept(
        pLinuxServerSocketObject->fd,
        (struct sockaddr *)&pLinuxServerSocketObject->address,
        (socklen_t*)&addrlen
    );
    if(socket_descriptor<0) perror("socket accept");
    LinuxSocket* pLinuxSocket = new_LinuxSocket();
    construct_LinuxSocket(pLinuxSocket, socket_descriptor);
    return &pLinuxSocket->pBaseObject;
}