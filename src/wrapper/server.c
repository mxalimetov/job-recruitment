
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <netinet/in.h>
#include <sys/socket.h>

#include "adapter/linux_server_socket.h"
#include "domain/socket.h"

#define PORT 8080

int main(int argc, char const *argv[])
{
    ServerSocket* pServerSocket = (ServerSocket*) new_LinuxServerSocket();
    construct_LinuxServerSocket((LinuxServerSocket*) pServerSocket, 8080);
    
    char buffer[1024] = {0};
    Socket* pSocket;

    printf("waiting for client\n");
    if( (pSocket = pServerSocket->accept(pServerSocket)) < 0) {
        perror("accept");
        exit(EXIT_FAILURE);
    }
    int len = pSocket->recv(pSocket, buffer, 1024);
    printf("%s\n", buffer);
    pSocket->send(pSocket, buffer, len);
    destruct_Socket(pSocket);
    destruct_ServerSocket(pServerSocket);
    return 0;
}