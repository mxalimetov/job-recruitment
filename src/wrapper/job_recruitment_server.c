
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <cjson/cJSON.h>

#include "adapter/linux_server_socket.h"
#include "adapter/inmemory_job_repository.h"
#include "adapter/sqlite_job_repository.h"
#include "domain/socket.h"
#include "domain/job_recruitment_server.h"
#include "domain/job_repository.h"

#define PORT 8080

int main(int argc, char const *argv[]) {
    printf("Application has started\n");

    ServerSocket* pServerSocket = (ServerSocket*) new_LinuxServerSocket();
    construct_LinuxServerSocket((LinuxServerSocket*) pServerSocket, 8080);

    // JobRepository* pJobRepository = (JobRepository*) new_InMemoryJobRepository();
    // construct_InMemoryJobRepository((InMemoryJobRepository*) pJobRepository);
    JobRepository* pJobRepository = (JobRepository*) new_SQLiteJobRepository();
    construct_SQLiteJobRepository((SQLiteJobRepository*) pJobRepository, "test.db");
    
    JobRecruitmentServer* pJobRecruitmentServer = new_JobRecruitmentServer();
    construct_JobRecruitmentServer(
        pJobRecruitmentServer,
        pServerSocket,
        pJobRepository
    );

    JobRecruitmentServer_main(pJobRecruitmentServer);

    destruct_Destructable((Destructable*) pJobRecruitmentServer);
    destruct_Destructable((Destructable*) pJobRepository);
    destruct_ServerSocket(pServerSocket);
    return 0;
}