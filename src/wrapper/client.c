
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <cjson/cJSON.h>

#include "domain/job.h"
#include "domain/socket.h"
#include "adapter/linux_client_socket.h"

void read_input(char* requestLine, char* default_, char* result, int bufferLength) {
    printf(requestLine, default_);
    fflush(stdout);
    if (fgets(result, bufferLength, stdin) != NULL) {
        char *newline = strchr(result, '\n');
        if (newline) *newline = 0;

        if (strlen(result) == 0) strcpy(result, default_);
    }
}

int main(int argc, char const *argv[])
{
    char ipAddress[20];
    //152.70.249.195
    read_input("Please input ip address(%s):", "127.0.0.1", ipAddress, 20);

    char stringPort[20];
    read_input("Please input port(%s):", "8080", stringPort, 20);
    int port = atoi(stringPort);

    while(1) { 
        printf("What do you want to do?\n");
        printf("1. create job\n");
        printf("2. read one job by id\n");
        printf("3. read jobs by title\n");
        printf("4. exit\n");
        printf("Enter what you gotta enter:\n");
        int option;
        scanf("%d", &option);
        if(option==4) break;

        ClientSocket* pClientSocket = (ClientSocket*) new_LinuxClientSocket();
        construct_LinuxClientSocket((LinuxClientSocket*) pClientSocket);

        printf("Connecting to server\n");
        pClientSocket->connect(pClientSocket, ipAddress, port);
        //printf("Connection was successfull\n");
        Socket* pSocket = (Socket*) pClientSocket;
        char *requestLines;
        if(option==1) {
            char tmpStr[1024];
            Job tmpJob;
            construct_Job(&tmpJob);
            printf("Input salary:\n");
            scanf("%d", &tmpJob.salary);

            printf("Input title:\n");
            scanf("%s", tmpStr);
            //destruct_String(&tmpJob.title);
            construct_String(&tmpJob.title, tmpStr);

            printf("Input description:\n");
            scanf("%s", tmpStr);
            construct_String(&tmpJob.description, tmpStr);

            cJSON* tmp;
            Job_marshal_CJSON(&tmpJob, &tmp);
            cJSON* resultingObject = cJSON_CreateObject();
            cJSON_AddStringToObject(resultingObject, "method", "create_job");
            cJSON_AddItemToObject(resultingObject, "data", tmp);
            requestLines = cJSON_Print(resultingObject);
        } else if(option==2) {
            Job tmpJob;
            construct_Job(&tmpJob);
            printf("Input id:\n");
            scanf("%d", &tmpJob.id);

            cJSON* tmp;
            Job_marshal_CJSON(&tmpJob, &tmp);
            cJSON* resultingObject = cJSON_CreateObject();
            cJSON_AddStringToObject(resultingObject, "method", "read_job");
            cJSON_AddItemToObject(resultingObject, "data", tmp);
            requestLines = cJSON_Print(resultingObject);
        } else if(option==3) {
            Job tmpJob;
            construct_Job(&tmpJob);
            printf("Input title:\n");
            char tmpStr[1024];
            scanf("%s", tmpStr);
            construct_String(&tmpJob.title, tmpStr);

            cJSON* tmp;
            Job_marshal_CJSON(&tmpJob, &tmp);
            cJSON* resultingObject = cJSON_CreateObject();
            cJSON_AddStringToObject(resultingObject, "method", "read_jobs");
            cJSON_AddItemToObject(resultingObject, "data", tmp);
            requestLines = cJSON_Print(resultingObject);
        } else {
            printf("sorry not implemented yet, or you pressed wrong button\n");
            continue;
        }
        printf("%s\n", requestLines);
        pSocket->send(pSocket, requestLines, 1024);


        char responseLines[1024];
        int len = pSocket->recv(pSocket, responseLines, 1024);
        responseLines[len] = 0;
        printf("%s\n", responseLines);

        destruct_Destructable((Destructable*) pClientSocket);
    }
    
    return 0;
}