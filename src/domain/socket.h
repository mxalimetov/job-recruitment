#ifndef SOCKET_H
#define SOCKET_H

#include "destructable.h"

typedef struct _Socket Socket;

typedef int (*IOPtr) (Socket* const pSocketObject, void* buffer, int buffer_length);

typedef IOPtr RecvPtr;
typedef IOPtr SendPtr;

typedef struct _Socket
{
    Destructable destructable;
    RecvPtr recv;
    SendPtr send;
} Socket;

Socket* new_Socket();
void construct_Socket(Socket* pSocketObject, RecvPtr recv, SendPtr send, DestructPtr destruct);
void destruct_Socket(Socket* const pSocketObject);

// int Socket_recv(Socket* const pSocketObject, void* buffer, int buffer_length);
// int Socket_send(Socket* const pSocketObject, void* buffer, int buffer_length);
#endif