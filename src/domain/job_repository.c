#include <stdlib.h>

#include "job_repository.h"

JobRepository* new_JobRepository() {
    JobRepository* pObject = (JobRepository*) malloc(sizeof(JobRepository));
    return pObject;
}

void construct_JobRepository(
    JobRepository* pJobRepositoryObject,
    JobRepository_CreatePtr pCreate,
    JobRepository_ReadPtr   pRead,
    JobRepository_ReadAllPtr   pReadAll,
    JobRepository_UpdatePtr pUpdate,
    JobRepository_DestructPtr pDelete,
    DestructPtr destruct
) {
    construct_Destructable(&pJobRepositoryObject->destructable, destruct);
    pJobRepositoryObject->pCreate = pCreate;
    pJobRepositoryObject->pRead = pRead;
    pJobRepositoryObject->pReadAll = pReadAll;
    pJobRepositoryObject->pUpdate = pUpdate;
    pJobRepositoryObject->pDelete = pDelete;
}

void destruct_JobRepository(JobRepository* const pJobRepositoryObject) {
    destruct_Destructable(&pJobRepositoryObject->destructable);
}