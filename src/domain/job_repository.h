#ifndef JOB_REPOSITORY_H
#define JOB_REPOSITORY_H

#include "destructable.h"
#include "job_node.h"

typedef struct _JobRepository JobRepository;

typedef void    (*JobRepository_CreatePtr)   (JobRepository* const pJobRepositoryObject, Job* pJob);
typedef void    (*JobRepository_ReadPtr)     (JobRepository* const pJobRepositoryObject, Job* pJob);
typedef void    (*JobRepository_ReadAllPtr)     (JobRepository* const pJobRepositoryObject, Job* pJob, JobNode** resultingList);
typedef void    (*JobRepository_UpdatePtr)   (JobRepository* const pJobRepositoryObject, Job* pJob);
typedef void    (*JobRepository_DestructPtr)   (JobRepository* const pJobRepositoryObject, Job* pJob);


typedef struct _JobRepository
{
    Destructable destructable;
    JobRepository_CreatePtr pCreate;
    JobRepository_ReadPtr   pRead;
    JobRepository_ReadAllPtr pReadAll;
    JobRepository_UpdatePtr pUpdate;
    JobRepository_DestructPtr pDelete;
} JobRepository;

JobRepository* new_JobRepository();
void construct_JobRepository(
    JobRepository* pJobRepositoryObject,
    JobRepository_CreatePtr pCreate,
    JobRepository_ReadPtr   pRead,
    JobRepository_ReadAllPtr   pReadAll,
    JobRepository_UpdatePtr pUpdate,
    JobRepository_DestructPtr pDelete,
    DestructPtr destruct
);
void destruct_JobRepository(JobRepository* const pJobRepositoryObject);


#endif