#ifndef JOB_NODE_H
#define JOB_NODE_H

#include "destructable.h"
#include "job.h"

typedef struct _JobNode JobNode;
typedef struct _JobNode
{
    Destructable destructable;
    Job current;
    JobNode* next;
} JobNode;

JobNode* new_JobNode();
void construct_JobNode(JobNode* const pObject, Job* pJob);
void destruct_JobNode(JobNode* const pObject);
void delete_JobNode(JobNode* const pObject);

#endif