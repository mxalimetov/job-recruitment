#include <stdlib.h>

#include "job.h"

Job* new_Job() {
    Job* pObject = (Job*) malloc(sizeof(Job));
    return pObject;
}
void construct_Job(Job* pJobObject) {
    construct_Destructable(&pJobObject->destructable, (DestructPtr) destruct_Job);
    pJobObject->id = 0;
    pJobObject->salary = 0;
    pJobObject->experience = 0;
    construct_String(&pJobObject->title, "");
    construct_String(&pJobObject->location, "");
    construct_String(&pJobObject->description, "");
    construct_String(&pJobObject->company, "");
}
void copy_Job(Job* pJobObject, Job* pJob) {
    construct_Job(pJobObject);
    pJobObject->id = pJob->id;
    pJobObject->salary = pJob->salary;
    pJobObject->experience = pJob->experience;
    copy_String(&pJobObject->title, &pJob->title);
    copy_String(&pJobObject->location, &pJob->location);
    copy_String(&pJobObject->description, &pJob->description);
    copy_String(&pJobObject->company, &pJob->company);
}

void destruct_Job(Job* const pJobObject) {
    destruct_Destructable((Destructable*) &pJobObject->title);
    destruct_Destructable((Destructable*) &pJobObject->location);
    destruct_Destructable((Destructable*) &pJobObject->description);
    destruct_Destructable((Destructable*) &pJobObject->company);
    //destruct_Destructable(&pJobObject->destructable);
}

void Job_unmarshal_CJSON(Job* const pJobObject, cJSON* pCJson) {
    construct_Job(pJobObject);
    if(pCJson==0) return;

    Number_unmarshal_CJSON(&pJobObject->id, cJSON_GetObjectItem(pCJson, "id"));
    Number_unmarshal_CJSON(&pJobObject->salary, cJSON_GetObjectItem(pCJson, "salary"));
    Number_unmarshal_CJSON(&pJobObject->experience, cJSON_GetObjectItem(pCJson, "experience"));

    String_unmarshal_CJSON(&pJobObject->title, cJSON_GetObjectItem(pCJson, "title"));
    String_unmarshal_CJSON(&pJobObject->location, cJSON_GetObjectItem(pCJson, "location"));
    String_unmarshal_CJSON(&pJobObject->description, cJSON_GetObjectItem(pCJson, "description"));
    String_unmarshal_CJSON(&pJobObject->company, cJSON_GetObjectItem(pCJson, "company"));
}
void Number_unmarshal_CJSON(int* const pObject, cJSON* pCJson) {
    if(cJSON_IsNumber(pCJson)) {
        *pObject = (int) cJSON_GetNumberValue(pCJson);
    } else {
        *pObject = 0;
    }
    
}
void Job_marshal_CJSON(Job* const pJobObject, cJSON** pCJson) {
    *pCJson = cJSON_CreateObject();
    cJSON_AddNumberToObject(*pCJson, "id", (double) pJobObject->id);
    cJSON_AddNumberToObject(*pCJson, "salary", (double) pJobObject->salary);
    cJSON_AddNumberToObject(*pCJson, "experience", (double) pJobObject->experience);

    cJSON_AddStringToObject(*pCJson, "title", pJobObject->title.str);
    cJSON_AddStringToObject(*pCJson, "location", pJobObject->location.str);
    cJSON_AddStringToObject(*pCJson, "description", pJobObject->description.str);
    cJSON_AddStringToObject(*pCJson, "company", pJobObject->company.str);
}