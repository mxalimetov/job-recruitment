#ifndef JOB_H
#define JOB_H

#include "destructable.h"
#include "string.h"
#include <cjson/cJSON.h>

typedef struct _Job
{
    Destructable destructable;
    int id;
    int salary;
    int experience;
    String title;
    String location;
    String description;
    String company;
} Job;

Job* new_Job();
void construct_Job(Job* pJobObject);
void copy_Job(Job* pJobObject, Job* pJob);
void destruct_Job(Job* const pJobObject);

void Job_unmarshal_CJSON(Job* const pJobObject, cJSON* pCJson);
void Number_unmarshal_CJSON(int* const pObject, cJSON* pCJson);
void Job_marshal_CJSON(Job* const pJobObject, cJSON** pCJson);

#endif