#include <stdlib.h>
#include <string.h>

#include "string.h"

String* new_String() {
    String* pObject = (String*) malloc(sizeof(String));
    return pObject;
}
void construct_String(String* pStringObject, const char* str) {
    construct_Destructable(&pStringObject->destructable, (DestructPtr) destruct_String);
    pStringObject->str = (char*) malloc(strlen(str));
    strcpy(pStringObject->str, str);
    
}
void destruct_String(String* const pStringObject) {
    free(pStringObject->str);
    destruct_Destructable(&pStringObject->destructable);
}

void copy_String(String* pStringObject, String* pString) {
    construct_String(pStringObject, pString->str);
}

void String_unmarshal_CJSON(String* const pStringObject, cJSON* pCJson) {
    if(cJSON_IsString(pCJson)) {
        construct_String(pStringObject, cJSON_GetStringValue(pCJson));
    } else {
        construct_String(pStringObject, "");
    }
}
void String_marshal_CJSON(String* const pStringObject, cJSON** pCJson) {
    *pCJson = cJSON_CreateString(pStringObject->str);
}