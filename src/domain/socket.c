#include "socket.h"
#include <stdlib.h>

Socket* new_Socket() {
    Socket* pObject = (Socket*) malloc(sizeof(Socket));
    return pObject;
}
void construct_Socket(Socket* pSocketObject, RecvPtr recv, SendPtr send, DestructPtr destruct) {
    pSocketObject->recv = recv;
    pSocketObject->send = send;
    construct_Destructable(&pSocketObject->destructable, destruct);
}
void destruct_Socket(Socket* const pSocketObject) {
    destruct_Destructable(&pSocketObject->destructable);
}