#include "destructable.h"
#include <stdlib.h>
#include <stdio.h>

Destructable* new_Destructable() {
    Destructable* pObject = (Destructable*) malloc(sizeof(Destructable));
    return pObject;
}
void construct_Destructable(Destructable* pDestructableObject, DestructPtr destruct) {
    pDestructableObject->pDestruct = destruct;
}
void destruct_Destructable(Destructable* const pDestructableObject) {
    pDestructableObject->pDestruct(pDestructableObject);
    //free(pDestructableObject);
}