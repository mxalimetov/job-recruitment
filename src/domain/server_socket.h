#ifndef SERVER_SOCKET_H
#define SERVER_SOCKET_H

#include "socket.h"

typedef struct _ServerSocket ServerSocket;
typedef Socket* (*AcceptPtr)   (ServerSocket* const pServerSocketObject);

typedef struct _ServerSocket
{
    Destructable destructable;
    AcceptPtr accept;

} ServerSocket;

ServerSocket* new_ServerSocket();
void construct_ServerSocket(ServerSocket* pServerSocketObject, AcceptPtr accept, DestructPtr destruct);
void destruct_ServerSocket(ServerSocket* const pServerSocketObject);

//int ServerSocket_accept(ServerSocket* const pServerSocketObject);
#endif