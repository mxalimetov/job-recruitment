#include "server_socket.h"
#include <stdlib.h>

ServerSocket* new_ServerSocket() {
    ServerSocket* pObject = (ServerSocket*) malloc(sizeof(ServerSocket));
    return pObject;
}
void construct_ServerSocket(ServerSocket* pServerSocketObject, AcceptPtr accept, DestructPtr destruct) {
    pServerSocketObject->accept = accept;
    construct_Destructable(&pServerSocketObject->destructable, destruct);
}
void destruct_ServerSocket(ServerSocket* const pServerSocketObject) {
    destruct_Destructable(&pServerSocketObject->destructable);
}