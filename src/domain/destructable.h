#ifndef DESTRUCTABLE_H
#define DESTRUCTABLE_H
typedef struct _Destructable Destructable;

typedef int (*DestructPtr) (Destructable* const pDestructableObject);

typedef struct _Destructable
{
    DestructPtr pDestruct;
} Destructable;

Destructable* new_Destructable();
void construct_Destructable(Destructable* pDestructableObject, DestructPtr destruct_);
void destruct_Destructable(Destructable* const pDestructableObject);
#endif