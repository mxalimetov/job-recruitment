#ifndef JOB_RECRUITMRNT_SERVER_H
#define JOB_RECRUITMRNT_SERVER_H

#include <cjson/cJSON.h>

#include "destructable.h"
#include "job.h"
#include "server_socket.h"
#include "job_repository.h"

typedef struct _JobRecruitmentServer JobRecruitmentServer;

typedef void    (*JobRecruitmentServer_CreateJobPtr)   (JobRecruitmentServer* const pObject, Job* pJob);
typedef void    (*JobRecruitmentServer_ReadJobPtr)   (JobRecruitmentServer* const pObject, Job* pJob);
typedef void    (*JobRecruitmentServer_UpdateJobPtr)   (JobRecruitmentServer* const pObject, Job* pJob);
typedef void    (*JobRecruitmentServer_DeleteJobPtr)   (JobRecruitmentServer* const pObject, Job* pJob);

typedef struct _JobRecruitmentServer
{
    Destructable destructable;
    JobRecruitmentServer_CreateJobPtr pCreateJob;
    JobRecruitmentServer_ReadJobPtr pReadJob;
    JobRecruitmentServer_UpdateJobPtr pUpdateJob;
    JobRecruitmentServer_DeleteJobPtr pDeleteJob;
    ServerSocket* pServerSocket;
    JobRepository* pJobRepository;

} JobRecruitmentServer;

JobRecruitmentServer* new_JobRecruitmentServer();
void construct_JobRecruitmentServer(
    JobRecruitmentServer* pJobRecruitmentServerObject,
    ServerSocket* pServerSocket,
    JobRepository* pJobRepository//,
    //DestructPtr destruct_
);
void destruct_JobRecruitmentServer(JobRecruitmentServer* const pJobRecruitmentServerObject);

void JobRecruitmentServer_main(JobRecruitmentServer* const pJobRecruitmentServerObject);
void JobRecruitmentServer_handleMethod(JobRecruitmentServer* const pJobRecruitmentServerObject, char* method, cJSON* data, cJSON** result);

#endif