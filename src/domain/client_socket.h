#ifndef CLIENT_SOCKET_H
#define CLIENT_SOCKET_H

#include "adapter/proxy_socket.h"

typedef struct _ClientSocket ClientSocket;
typedef int    (*ConnectPtr)   (ClientSocket*, char*, int);

typedef struct _ClientSocket
{
    ProxySocket pBaseObject;
    ConnectPtr connect;

} ClientSocket;

ClientSocket* new_ClientSocket();
void construct_ClientSocket(ClientSocket* pClientSocketObject, ConnectPtr connect);
void destruct_ClientSocket(ClientSocket* const pClientSocketObject);

//int ClientSocket_connect(ClientSocket* const pClientSocketObject);
#endif