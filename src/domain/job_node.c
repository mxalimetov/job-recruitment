#include <stdlib.h>

#include "job_node.h"

JobNode* new_JobNode() {
    JobNode* pObject = (JobNode*) malloc(sizeof(JobNode));
    return pObject;
}
void delete_JobNode(JobNode* const pObject) {
    free(pObject);
}
void construct_JobNode(JobNode* const pObject, Job* pJob) {
    construct_Destructable(&pObject->destructable, (DestructPtr) destruct_JobNode);
    copy_Job(&pObject->current, pJob);
    pObject->next = 0;
}

void destruct_JobNode(JobNode* const pObject) {
    destruct_Destructable((Destructable*) &pObject->current);
    //destruct_Destructable(&pObject->destructable);
}