#include "job_recruitment_server.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <arpa/inet.h>

#include "adapter/linux_server_socket.h"

JobRecruitmentServer* new_JobRecruitmentServer() {
    JobRecruitmentServer* pObject = (JobRecruitmentServer*) malloc(sizeof(JobRecruitmentServer));
    return pObject;
}
void construct_JobRecruitmentServer(
    JobRecruitmentServer* pJobRecruitmentServerObject,
    ServerSocket* pServerSocket,
    JobRepository* pJobRepository
    //DestructPtr destruct_
) {
    construct_Destructable(&pJobRecruitmentServerObject->destructable, (DestructPtr) destruct_JobRecruitmentServer);
    pJobRecruitmentServerObject->pServerSocket = pServerSocket;
    pJobRecruitmentServerObject->pJobRepository = pJobRepository;
}
void destruct_JobRecruitmentServer(JobRecruitmentServer* const pJobRecruitmentServerObject) {
    
}

void JobRecruitmentServer_main(JobRecruitmentServer* const pJobRecruitmentServerObject) {
    char buffer[1024];
    ServerSocket* pServerSocket = pJobRecruitmentServerObject->pServerSocket;
    Socket* pSocket;

    while (1) {
        //printf("waiting for client\n");
        if( (pSocket = pServerSocket->accept(pServerSocket)) < 0) {
            perror("accept");
            continue;
            //exit(EXIT_FAILURE);
        }
        // char buf[50];
        // inet_ntop(
        //     ((LinuxServerSocket*) pServerSocket)->fd,,
        //     ((LinuxServerSocket*) pServerSocket)->address.sin_addr,
        //     buf,
        //     sizeof(((LinuxServerSocket*) pServerSocket)->address)
        // );
        //printf("connection established\n");
        
        int len = pSocket->recv(pSocket, buffer, 1024);
        
        cJSON *pObject = cJSON_ParseWithLength(buffer, 1024);
        cJSON *resultObject;
        if (cJSON_IsObject(pObject)) {
            cJSON * method = cJSON_GetObjectItem(pObject, "method");
            if (cJSON_IsString(method)) {
                if(cJSON_HasObjectItem(pObject, "data")) {
                    JobRecruitmentServer_handleMethod(
                        pJobRecruitmentServerObject,
                        cJSON_GetStringValue(method),
                        cJSON_GetObjectItem(pObject, "data"),
                        &resultObject
                    );
                } else {
                    resultObject = cJSON_CreateObject();
                    cJSON_AddBoolToObject(resultObject, "ok", 0);
                }
            } else {
                resultObject = cJSON_CreateObject();
                cJSON_AddBoolToObject(resultObject, "ok", 0);
            }
        } else {
            resultObject = cJSON_CreateObject();
            cJSON_AddBoolToObject(resultObject, "ok", 0);
        }
        char* tmpBuffer = cJSON_Print(resultObject);
        pSocket->send(pSocket, tmpBuffer, strlen(tmpBuffer));
        destruct_Socket(pSocket);
        free(pSocket);
    }
}

void JobRecruitmentServer_handleMethod(
    JobRecruitmentServer* const pJobRecruitmentServerObject,
    char* method,
    cJSON* data,
    cJSON** resultObject
) {
    printf("connection established, %s executed\n", method);
    if(strcmp("create_job", method)==0) {
        Job job;
        Job_unmarshal_CJSON(&job, data);
        //pJobRecruitmentServerObject->pCreateJob(pJobRecruitmentServerObject, &job);
        pJobRecruitmentServerObject->pJobRepository->pCreate(
            pJobRecruitmentServerObject->pJobRepository,
            &job
        );
        Job_marshal_CJSON(&job, resultObject);
    } else if(strcmp("read_job", method)==0) {
        Job job;
        Job_unmarshal_CJSON(&job, data);
        pJobRecruitmentServerObject->pJobRepository->pRead(
            pJobRecruitmentServerObject->pJobRepository,
            &job
        );
        //pJobRecruitmentServerObject->pCreateJob(pJobRecruitmentServerObject, &job);
        Job_marshal_CJSON(&job, resultObject);
    } else if(strcmp("read_jobs", method)==0) {
        Job job;
        Job_unmarshal_CJSON(&job, data);
        JobNode* tmp;
        pJobRecruitmentServerObject->pJobRepository->pReadAll(
            pJobRecruitmentServerObject->pJobRepository,
            &job, &tmp
        );
        *resultObject = cJSON_CreateArray();
        while(tmp!=0) {
            cJSON *tmpObject;
            Job_marshal_CJSON(&tmp->current, &tmpObject);
            cJSON_AddItemToArray(*resultObject, tmpObject);
            tmp = tmp->next;
        }
        //pJobRecruitmentServerObject->pCreateJob(pJobRecruitmentServerObject, &job);
        //Job_marshal_CJSON(&job, resultObject);
    } else {
        *resultObject = cJSON_CreateObject();
        cJSON_AddBoolToObject(*resultObject, "ok", 0);
    }
}