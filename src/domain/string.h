#ifndef STRING_H
#define STRING_H

#include "destructable.h"
#include <cjson/cJSON.h>

typedef struct _String String;

typedef struct _String
{
    Destructable destructable;
    char* str;
} String;

String* new_String();
void construct_String(String* pStringObject, const char* str);
void copy_String(String* pStringObject, String* pString);
void destruct_String(String* const pStringObject);

void String_unmarshal_CJSON(String* const pStringObject, cJSON* pCJson);
void String_marshal_CJSON(String* const pStringObject, cJSON** pCJson);

#endif